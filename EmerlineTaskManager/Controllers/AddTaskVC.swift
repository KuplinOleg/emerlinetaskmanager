//
//  AddTaskVC.swift
//  EmerlineTaskManager
//
//  Created by Oleg Kuplin on 23.11.2017.
//  Copyright © 2017 Oleg Kuplin. All rights reserved.
//

import UIKit

class AddTaskVC: UITableViewController, UIGestureRecognizerDelegate {
    
    @IBOutlet weak var errorLabel: UILabel!
    @IBOutlet weak var taskNameTF: UITextField!
    @IBOutlet weak var startDateTF: UITextField!
    @IBOutlet weak var dueDateTF: UITextField!
    @IBOutlet weak var monthPicker: UIPickerView!
    @IBOutlet weak var weekPicker: UIPickerView!
    @IBOutlet weak var dayPicker: UIPickerView!
    @IBOutlet weak var progressTF: UITextField!
    @IBOutlet weak var progressSlider: UISlider!
    @IBOutlet weak var newStateButton: UIButton!
    @IBOutlet weak var inProgressStateButton: UIButton!
    @IBOutlet weak var doneStateButton: UIButton!
    
    
    let datePicker = UIDatePicker()
    let dateFormatter = DateFormatter().taskDateFormatter!
    var monthPickerData = [Int](), weekPickerData = [Int](), dayPickerData = [Int]()
    var editingTask: TaskMO!, selectedState = "", stateButtons = [UIButton]()

    override func viewDidLoad() {
        super.viewDidLoad()

        navigationController?.interactivePopGestureRecognizer?.delegate = self
        navigationItem.title = editingTask == nil ? "Add New Task" : "Edit task"
        let saveButton = UIBarButtonItem(title: "Save", style: .plain, target: self, action: #selector(saveTask))
        navigationItem.rightBarButtonItem = saveButton
        
        stateButtons = [newStateButton, inProgressStateButton, doneStateButton]
        setTextFields()
        setPickersData()
        
        if editingTask != nil {
            taskNameTF.text  = editingTask.name!
            startDateTF.text = editingTask.startDate!.stringValue!
            dueDateTF.text   = editingTask.dueDate!.stringValue!
            dayPicker.selectRow(editingTask.estimatedDays, inComponent: 0, animated: false)
            weekPicker.selectRow(editingTask.estimatedWeeks, inComponent: 0, animated: false)
            monthPicker.selectRow(editingTask.estimatedMonths, inComponent: 0, animated: false)
            progressTF.text = "\(editingTask.percent!)"
            progressSlider.setValue(Float(editingTask.percent), animated: false)
            for button in stateButtons where button.titleLabel!.text! == editingTask.state! {
                changeState(button)
                break
            }
            navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.interactivePopGestureRecognizer?.isEnabled = true
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return editingTask == nil ? 4 : 6
    }
    
    @objc func saveTask() {
        if taskNameTF.text!.isEmpty {
            errorLabel.isHidden = false
            errorLabel.text = "Please write task name"
        } else if startDateTF.text!.isEmpty {
            errorLabel.isHidden = false
            errorLabel.text = "Please choose start date"
        } else if dueDateTF.text!.isEmpty {
            errorLabel.isHidden = false
            errorLabel.text = "Please choose due date"
        } else {
            var title = "You sure you want to save new task?"
            if editingTask != nil {
                title = "Do you want to save changes?"
            }
            let alert = UIAlertController(title: title, message: "", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "No", style: .cancel, handler: nil))
            alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { (action) in
                if self.editingTask == nil {
                    self.saveNewTask()
                } else {
                    self.editTask()
                }
            }))
            present(alert, animated: true, completion: nil)
        }
    }

    func saveNewTask() {
        let name = taskNameTF.text!
        let startDate = dateFormatter.date(from: startDateTF.text!)!
        let dueDate = dateFormatter.date(from: dueDateTF.text!)!
        let estimatedTime = "\(dayPicker.selectedRow(inComponent: 0)) \(weekPicker.selectedRow(inComponent: 0)) \(monthPicker.selectedRow(inComponent: 0))"
        DataController.shared.saveTask(name: name, id: 0, estimatedTime: estimatedTime, startDate: startDate, dueDate: dueDate)
        navigationController?.popViewController(animated: true)
    }
    
    func editTask() {
        let name = taskNameTF.text!
        let startDate = dateFormatter.date(from: startDateTF.text!)!
        let dueDate = dateFormatter.date(from: dueDateTF.text!)!
        let estimatedTime = "\(dayPicker.selectedRow(inComponent: 0)) \(weekPicker.selectedRow(inComponent: 0)) \(monthPicker.selectedRow(inComponent: 0))"
        let completionPercent = Int16(progressTF.text!)!
        
        editingTask.name = name
        editingTask.startDate = startDate
        editingTask.dueDate = dueDate
        editingTask.estimatedTime = estimatedTime
        editingTask.completionPercent = completionPercent
        editingTask.state = selectedState
        DataController.shared.saveContext()
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func changeState(_ sender: Any) {
        guard case let selectedButton as UIButton = sender else { return }
        selectedButton.isSelected = true
        selectedState = selectedButton.titleLabel!.text!
        for button in stateButtons where button != selectedButton {
            button.isSelected = false
        }
        if selectedButton == doneStateButton {
            progressSlider.setValue(100, animated: true)
            progressTF.text = "100"
        } else if selectedButton == newStateButton {
            progressSlider.setValue(0, animated: true)
            progressTF.text = "0"
        }
    }
    
    //This function create toolbars for textFields and convenient date picker and set it as inputView to startDateTF and dueDateTF
    func setTextFields() {
        datePicker.minimumDate = Date()
        let toolBar = UIToolbar()
        toolBar.sizeToFit()
        let doneButton = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(picDate(_:)))
        let flexSpace  = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        toolBar.setItems([flexSpace, doneButton], animated: false)
        datePicker.datePickerMode = .date
        startDateTF.inputAccessoryView = toolBar
        startDateTF.inputView = datePicker
        dueDateTF.inputAccessoryView = toolBar
        dueDateTF.inputView = datePicker
        let resignToolBar = UIToolbar()
        resignToolBar.sizeToFit()
        let resignDoneButton = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(endEditing(_:)))
        resignToolBar.setItems([flexSpace, resignDoneButton], animated: false)
        progressTF.inputAccessoryView = resignToolBar

    }
    
    @objc func picDate(_ sender: UIBarButtonItem) {
        let date = dateFormatter.string(from: datePicker.date)
        if startDateTF.isFirstResponder {
            startDateTF.text = date
        } else {
            dueDateTF.text = date
        }
        if !startDateTF.text!.isEmpty && !dueDateTF.text!.isEmpty {
            let fromDate = dateFormatter.date(from: startDateTF.text!)!
            let toDate = dateFormatter.date(from: dueDateTF.text!)!
            let comparedDate =  Calendar.current.dateComponents([.day, .month], from: fromDate, to: toDate)
            let days = comparedDate.day! % 7
            let months = comparedDate.month!
            let weeks = comparedDate.day! / 7
            if days > 0 {
                dayPicker.selectRow(days, inComponent: 0, animated: true)
            } else {
                dayPicker.selectRow(0, inComponent: 0, animated: true)
            }
            if months > 0 {
                monthPicker.selectRow(months, inComponent: 0, animated: true)
            } else {
                monthPicker.selectRow(0, inComponent: 0, animated: true)
            }
            if weeks > 0 {
                weekPicker.selectRow(weeks, inComponent: 0, animated: true)
            } else {
                weekPicker.selectRow(0, inComponent: 0, animated: true)
            }
        }
        view.endEditing(true)
    }
    
    func setPickersData() {
        for i in 0..<37 {
            monthPickerData.append(i)
        }
        for i in 0..<6 {
            weekPickerData.append(i)
        }
        for i in 0..<32 {
            dayPickerData.append(i)
        }
    }
    
    @IBAction func setProgress(_ sender: Any) {
        progressTF.text = "\(Int(progressSlider.value))"
        checkProgress()
    }
    
    @IBAction func endEditing(_ sender: Any) {
        progressSlider.setValue(Float(progressTF.text!)!, animated: true)
        checkProgress()
        view.endEditing(true)
    }
    
    func checkProgress() {
        let progress = Int(progressTF.text!)
        if progress == 100 {
            changeState(doneStateButton)
        } else {
            changeState(inProgressStateButton)
        }
    }
    
    
}


extension AddTaskVC: UIPickerViewDelegate, UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView == monthPicker {
            return monthPickerData.count
        } else if pickerView == weekPicker {
            return weekPickerData.count
        } else if pickerView == dayPicker {
            return dayPickerData.count
        } else { return 0 }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView == monthPicker {
            return "\(monthPickerData[row])"
        } else if pickerView == weekPicker {
            return "\(weekPickerData[row])"
        } else if pickerView == dayPicker {
            return "\(dayPickerData[row])"
        } else { return "" }
    }
    
}

extension AddTaskVC: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if !textField.text!.isEmpty {
            guard let date = dateFormatter.date(from: textField.text!) else { return }
            datePicker.setDate(date, animated: true)
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if taskNameTF.text!.isEmpty {
            taskNameTF.becomeFirstResponder()
        } else if startDateTF.text!.isEmpty {
            startDateTF.becomeFirstResponder()
        } else if dueDateTF.text!.isEmpty {
            dueDateTF.becomeFirstResponder()
        }
    }
    
}


















