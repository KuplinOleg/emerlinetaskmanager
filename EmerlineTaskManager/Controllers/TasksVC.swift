//
//  TasksVC.swift
//  EmerlineTaskManager
//
//  Created by Oleg Kuplin on 23.11.2017.
//  Copyright © 2017 Oleg Kuplin. All rights reserved.
//

import UIKit

class TasksVC: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    
    var tasks = [TaskMO]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.tableFooterView = UIView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tasks = DataController.shared.getTasksMO()
        tableView.reloadData()
    }
    
    @IBAction func addNewTask(_ sender: Any) {
        moveToAddTaskVC()
    }
    
    func moveToAddTaskVC(with task: TaskMO? = nil) {
        let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let addTaskVC = mainStoryboard.instantiateViewController(withIdentifier: "AddTaskViewController") as! AddTaskVC
        addTaskVC.editingTask = task
        navigationController?.pushViewController(addTaskVC, animated: true)
    }

}

extension TasksVC: UITableViewDataSource, UITableViewDelegate {
    
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 20
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return tasks.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "tasksCell") as! TaskCell
        cell.task = tasks[indexPath.section]
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 180
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        moveToAddTaskVC(with: tasks[indexPath.section])
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            DataController.shared.deleteTask(tasks[indexPath.section])
            tasks.remove(at: indexPath.row)
            tableView.beginUpdates()
            tableView.deleteSections(IndexSet(integer: indexPath.section), with: .fade)
            tableView.endUpdates()
        }
    }
    
}


































