//
//  Extensions.swift
//  EmerlineTaskManager
//
//  Created by Oleg Kuplin on 23.11.2017.
//  Copyright © 2017 Oleg Kuplin. All rights reserved.
//

import Foundation


extension Date {
    var stringValue: String? {
        return DateFormatter().taskDateFormatter.string(from: self)
    }
}

extension DateFormatter {
    
    var taskDateFormatter: DateFormatter! {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy.MM.dd"
        return dateFormatter
    }
    
}
