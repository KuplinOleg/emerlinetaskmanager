//
//  DataController.swift
//  PopCarpet
//
//  Created by Oleg Kuplin on 31.10.2017.
//  Copyright © 2017 Oleg Kuplin. All rights reserved.
//

import Foundation
import UIKit
import CoreData
class DataController: NSObject {
    
    static let shared = DataController()
    var managedObjectContext: NSManagedObjectContext
    
    var applicationDocumentsDirectory: URL = {
        // The directory the application uses to store the Core Data store file. This code uses a directory named "com.cadiridris.coreDataTemplate" in the application's documents Application Support directory.
        let urls = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return urls[urls.count-1]
    }()
    
    override init() {
        //This resource is the same name as your xcdatamodeld contained in your project
        guard let modelURL = Bundle.main.url(forResource: "EmerlineTaskManager", withExtension:"momd") else {
            fatalError("Error loading model from bundle")
        }
        // The managed object model for the application. It is a fatal error for the application not to be able to find and load its model.
        guard let mom = NSManagedObjectModel(contentsOf: modelURL) else {
            fatalError("Error initializing mom from: \(modelURL)")
        }
        
        let psc = NSPersistentStoreCoordinator(managedObjectModel: mom)
        let url = applicationDocumentsDirectory.appendingPathComponent("SingleViewCoreData.sqlite")
        do {
            try psc.addPersistentStore(ofType: NSSQLiteStoreType, configurationName: nil, at: url, options: nil)
        } catch {
            print("Error while saving persistent store occured")
            abort()
        }
        
        managedObjectContext = NSManagedObjectContext(concurrencyType: NSManagedObjectContextConcurrencyType.mainQueueConcurrencyType)
        managedObjectContext.persistentStoreCoordinator = psc
        
        let queue = DispatchQueue.global(qos: DispatchQoS.QoSClass.background)
        queue.async {
            guard let docURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).last else {
                fatalError("Unable to resolve document directory")
            }
            let storeURL = docURL.appendingPathComponent("DataModel.sqlite")
            do {
                try psc.addPersistentStore(ofType: NSSQLiteStoreType, configurationName: nil, at: storeURL, options: nil)
            } catch {
                fatalError("Error migrating store: \(error)")
            }
        }
    }
    
    internal func saveContext() {
        do {
            try managedObjectContext.save()
        } catch {
            fatalError("Failure to save context: \(error)")
        }
    }
    
    internal func saveTask(name: String,
                           id: Int,
                           estimatedTime: String,
                           startDate: Date,
                           dueDate: Date) {
        
        let newTask = NSEntityDescription.insertNewObject(forEntityName: "Task", into: managedObjectContext) as! TaskMO
        
        newTask.id = Int32(id)
        newTask.name = name
        newTask.estimatedTime = estimatedTime
        newTask.startDate = startDate
        newTask.dueDate = dueDate
    
        saveContext()
    }

    internal func getTasksMO() -> [TaskMO] {
        let tasksFetch = NSFetchRequest<NSFetchRequestResult>(entityName: "Task")
        do {
            let fetchedTasks = try managedObjectContext.fetch(tasksFetch) as! [TaskMO]
            return fetchedTasks
        } catch {
            fatalError("Failed to fetch tasks: \(error)")
        }
    }

    internal func deleteTask(_ task: TaskMO) {
        managedObjectContext.delete(task)
        saveContext()
    }

}

































