//
//  TaskMO.swift
//  EmerlineTaskManager
//
//  Created by Oleg Kuplin on 23.11.2017.
//  Copyright © 2017 Oleg Kuplin. All rights reserved.
//

import Foundation
import UIKit

public enum TaskState: String {
    case New, InProgress, Done
    
    var color: UIColor! {
        switch self {
        case .New:
            return #colorLiteral(red: 0.2392156869, green: 0.6745098233, blue: 0.9686274529, alpha: 1)
        case .InProgress:
            return #colorLiteral(red: 0.9686274529, green: 0.78039217, blue: 0.3450980484, alpha: 1)
        case .Done:
            return #colorLiteral(red: 0.2980392157, green: 0.8470588235, blue: 0.3921568627, alpha: 1)
        }
    }
    
}

extension TaskMO {
    
    var taskState: TaskState! {
        return TaskState.init(rawValue: state!.replacingOccurrences(of: " ", with: "")) ?? .Done
    }
    
    var percent: Int! {
        return Int(completionPercent)
    }
    
    var estimatedDays: Int! {
        return Int(estimatedTime!.components(separatedBy: " ")[0])
    }
    
    var estimatedWeeks: Int! {
        return Int(estimatedTime!.components(separatedBy: " ")[1])
    }
    
    var estimatedMonths: Int! {
        return Int(estimatedTime!.components(separatedBy: " ")[2])
    }
    
}

















