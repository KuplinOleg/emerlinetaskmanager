//
//  TaskCell.swift
//  EmerlineTaskManager
//
//  Created by Oleg Kuplin on 23.11.2017.
//  Copyright © 2017 Oleg Kuplin. All rights reserved.
//

import UIKit

class TaskCell: UITableViewCell {

    @IBOutlet weak var taskNameLabel: UILabel!
    @IBOutlet weak var progressLabel: UILabel!
    @IBOutlet weak var progressSlider: UISlider!
    @IBOutlet weak var stateLabel: UILabel!
    @IBOutlet weak var startDateLabel: UILabel!
    @IBOutlet weak var dueDateLabel: UILabel!
    @IBOutlet weak var estimatedTimeLabel: UILabel!
    
    
    var task: TaskMO! {
        didSet {
            let percent = task.percent!
            //This if statement needed to remove rest (left or right) of slider line
            progressSlider.minimumTrackTintColor = #colorLiteral(red: 0.2980392157, green: 0.8470588235, blue: 0.3921568627, alpha: 1)
            progressSlider.maximumTrackTintColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
            if percent == 0 {
                progressSlider.minimumTrackTintColor = .clear
            } else if percent == 100 {
                progressSlider.maximumTrackTintColor = .clear
            }
            progressSlider.setValue(Float(percent), animated: false)
            taskNameLabel.text   = task.name!
            progressLabel.text   = "\(percent)%"
            stateLabel.text      = task.state!
            stateLabel.textColor = task.taskState.color
            startDateLabel.text  = "Start date: \(task.startDate!.stringValue!)"
            dueDateLabel.text    = "Due date:  \(task.dueDate!.stringValue!)"
            let days = task.estimatedDays!
            let weeks = task.estimatedWeeks!
            let months = task.estimatedMonths!
            var estimatedTime = ""
            if days != 0 {
                estimatedTime = "Days: \(days)"
            }
            if weeks != 0 {
                estimatedTime = "Weeks: \(weeks) " + estimatedTime
            }
            if months != 0 {
                estimatedTime = "Months: \(months) " + estimatedTime
            }
            estimatedTime = "Estimated time:\n" + estimatedTime
            estimatedTimeLabel.text = estimatedTime
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
